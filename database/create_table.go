package database

import "github.com/ssst0n3/awesome_libs/awesome_error"

func CreateTableTeam() (err error) {
	query := `create table if not exists team(
    id    integer primary key autoincrement,
    name  text,
    score integer
);`
	_, err = Conn.Exec(query)
	awesome_error.CheckErr(err)
	return
}

func CreateTableChallenge() (err error) {
	query := `create table if not exists challenge(
    id    integer primary key autoincrement,
    name  text,
    score integer
);`
	_, err = Conn.Exec(query)
	awesome_error.CheckErr(err)
	return
}

func CreateTableMachine() (err error) {
	query := `create table if not exists machine(
    id    integer primary key autoincrement,
    ip  text,
    port integer,
    username  text,
    password  text,
    challenge_id  integer,
    team_id integer
);`
	_, err = Conn.Exec(query)
	awesome_error.CheckErr(err)
	return
}

func CreateTableAttack() (err error)  {
	query := `create table if not exists attack(
    id    integer primary key autoincrement,
	hash text,
	team_id integer,
	challenge_id integer,
	round integer,
	victim_id integer
);`
	_, err = Conn.Exec(query)
	awesome_error.CheckErr(err)
	return
}