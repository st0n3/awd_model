package database

import (
	"github.com/ssst0n3/awesome_libs/awesome_error"
	"github.com/ssst0n3/lightweight_api/example/resource/kv_config"
	"github.com/ssst0n3/lightweight_db"
	"time"
)

func GetTime(key string) (t time.Time, err error) {
	value, err := kv_config.GetValueByKey(key)
	if err != nil {
		awesome_error.CheckErr(err)
		return
	}
	t, err = time.Parse(time.RFC3339, value)
	awesome_error.CheckErr(err)
	return
}

func Set(key string, value string) (err error) {
	err = Conn.UpdateObjectSingleColumnByGuid(lightweight_db.ColumnNameConfigKey, key, lightweight_db.TableNameConfig, lightweight_db.ColumnNameConfigValue, value)
	awesome_error.CheckErr(err)
	return
}

func SetTime(key string, t time.Time) (err error) {
	value := t.Format(time.RFC3339)
	return Set(key, value)
}

func CreateEmpty(key, value string) {
	Conn.CreateObject(lightweight_db.TableNameConfig, lightweight_db.Config{
		Key:   key,
		Value: value,
	})
}
