package database

import (
	"github.com/ssst0n3/awesome_libs/awesome_error"
	"github.com/ssst0n3/lightweight_db"
	"os"
)

func init() {
	path := "../db/awd_model.sqlite"
	os.Remove(path)
	awesome_error.CheckFatal(os.Setenv(lightweight_db.EnvDbDsn, path))
	Init()
}
