package database

import (
	"github.com/ssst0n3/awesome_libs/awesome_error"
	"github.com/ssst0n3/lightweight_api"
	"github.com/ssst0n3/lightweight_db"
	"github.com/ssst0n3/lightweight_db/example/sqlite"
)

var Conn lightweight_db.Connector

func Init() {
	Conn = sqlite.Conn()
	lightweight_api.Conn = Conn
	awesome_error.CheckFatal(Conn.CreateTableConfig())
	awesome_error.CheckFatal(CreateTableTeam())
	awesome_error.CheckFatal(CreateTableChallenge())
	awesome_error.CheckFatal(CreateTableMachine())
	awesome_error.CheckFatal(CreateTableAttack())
}
