package model

type Machine struct {
	Ip          string `json:"ip"`
	Port        uint   `json:"port"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	ChallengeId uint   `json:"challenge_id"`
	TeamId      uint   `json:"team_id"`
}

const (
	TableNameMachine = "machine"
)
