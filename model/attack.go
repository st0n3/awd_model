package model

type Attack struct {
	Hash        string `json:"hash"`
	TeamId      uint   `json:"team_id"`
	ChallengeId uint   `json:"challenge_id"`
	Round       uint   `json:"round"`
	VictimId    uint   `json:"victim_id"`
}

const (
	TableNameAttack = "attack"
	ColumnNameAttackHash = "hash"
)
