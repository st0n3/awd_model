package model

type Team struct {
	Name  string `json:"name"`
	Score int    `json:"score"`
}

const (
	TableNameTeam = "team"
)
