package model

import "time"

type Event struct {
	Name          string    `json:"name"`
	StartTime     time.Time `json:"start_time"`
	EndTime       time.Time `json:"end_time"`
	RoundInterval int       `json:"round_interval"`
}
