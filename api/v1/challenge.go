package v1

import (
	"github.com/ssst0n3/lightweight_api"
	"gitlab.com/st0n3/awd_model/model"
)

var ChallengeResource = lightweight_api.Resource{
	Name:             model.TableNameChallenge,
	TableName:        model.TableNameChallenge,
	BaseRelativePath: "/api/v1/" + model.TableNameChallenge,
	Model:            model.Challenge{},
	GuidFieldJsonTag: model.ColumnNameChallengeName,
}
