package v1

import (
	"github.com/ssst0n3/lightweight_api"
	"gitlab.com/st0n3/awd_model/model"
)

var AttackResource = lightweight_api.Resource{
	Name:             model.TableNameAttack,
	TableName:        model.TableNameAttack,
	BaseRelativePath: "/api/v1/" + model.TableNameAttack,
	Model:            model.Attack{},
	GuidFieldJsonTag: model.ColumnNameAttackHash,
}
