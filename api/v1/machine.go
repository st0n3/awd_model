package v1

import (
	"github.com/ssst0n3/lightweight_api"
	"gitlab.com/st0n3/awd_model/model"
)

var MachineResource = lightweight_api.Resource{
	Name:             model.TableNameMachine,
	TableName:        model.TableNameMachine,
	BaseRelativePath: "/api/v1/" + model.TableNameMachine,
	Model:            model.Machine{},
	GuidFieldJsonTag: "",
}
