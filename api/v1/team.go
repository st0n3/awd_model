package v1

import (
	"github.com/ssst0n3/lightweight_api"
	"gitlab.com/st0n3/awd_model/model"
)

var TeamResource = lightweight_api.Resource{
	Name:             model.TableNameTeam,
	TableName:        model.TableNameTeam,
	BaseRelativePath: "/api/v1/" + model.TableNameTeam,
	Model:            model.Team{},
	GuidFieldJsonTag: "",
}
