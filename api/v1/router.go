package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/ssst0n3/awesome_libs/awesome_error"
	"github.com/ssst0n3/lightweight_api/example/resource/initialize"
	"github.com/ssst0n3/lightweight_api/example/resource/kv_config"
	"gitlab.com/st0n3/awd_model/database"
)

func InitRouter(router *gin.Engine) {
	initialize.FlagUseShouldInitialize = true
	var err error
	if initialize.ShouldInitialize, err = database.Conn.ShouldInitialize(); err != nil {
		awesome_error.CheckFatal(err)
	}
	initialize.InitRouter(router)
	kv_config.InitRouter(router)
	machineGroup := router.Group(MachineResource.BaseRelativePath)
	{
		machineGroup.GET("", MachineResource.ListResource)
		machineGroup.GET("/:id", MachineResource.ShowResource)
		machineGroup.POST("", MachineResource.CreateResource)
		machineGroup.PUT("/:id", MachineResource.UpdateResource)
		machineGroup.DELETE("/:id", MachineResource.DeleteResource)
	}
	teamGroup := router.Group(TeamResource.BaseRelativePath)
	{
		teamGroup.GET("", TeamResource.ListResource)
		teamGroup.GET("/:id", TeamResource.ShowResource)
		teamGroup.POST("", TeamResource.CreateResource)
		teamGroup.PUT("/:id", TeamResource.UpdateResource)
		teamGroup.DELETE("/:id", TeamResource.DeleteResource)
	}
	challengeGroup := router.Group(ChallengeResource.BaseRelativePath)
	{
		challengeGroup.GET("", ChallengeResource.ListResource)
		challengeGroup.GET("/:id", ChallengeResource.ShowResource)
		challengeGroup.POST("", ChallengeResource.CreateResource)
		challengeGroup.PUT("/:id", ChallengeResource.UpdateResource)
		challengeGroup.DELETE("/:id", ChallengeResource.DeleteResource)
	}
	attackGroup := router.Group(AttackResource.BaseRelativePath)
	{
		attackGroup.GET("", AttackResource.ListResource)
		attackGroup.GET("/:id", AttackResource.ShowResource)
		attackGroup.POST("/", AttackResource.CreateResource)
		attackGroup.PUT("/:id", AttackResource.UpdateResource)
		attackGroup.DELETE("/:id", AttackResource.DeleteResource)
	}
}
