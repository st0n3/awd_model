package router

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	v1 "gitlab.com/st0n3/awd_model/api/v1"
	"net/http"
)

func InitRouter() *gin.Engine {
	router := gin.Default()
	{
		// cors
		corsConfig := cors.DefaultConfig()
		corsConfig.AllowAllOrigins = true
		corsConfig.AllowCredentials = false
		router.Use(cors.New(corsConfig))
	}
	{
		// frontend
		router.Use(static.Serve("/", static.LocalFile("./dist", false)))
	}
	{
		// ping
		router.GET("/ping", func(context *gin.Context) {
			context.JSON(http.StatusOK, gin.H{"message": "pong"})
		})
	}
	{
		v1.InitRouter(router)
	}
	return router
}
