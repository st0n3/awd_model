package main

import (
	"github.com/ssst0n3/awesome_libs/awesome_error"
	"gitlab.com/st0n3/awd_model/config"
	"gitlab.com/st0n3/awd_model/database"
	"gitlab.com/st0n3/awd_model/router"
)

func main() {
	database.Init()
	awesome_error.CheckErr(config.Init())
	r := router.InitRouter()
	awesome_error.CheckFatal(r.Run(":13300"))
}
