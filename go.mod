module gitlab.com/st0n3/awd_model

go 1.14

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/static v0.0.0-20200916080430-d45d9a37d28e
	github.com/gin-gonic/gin v1.6.3
	github.com/ssst0n3/awesome_libs v0.5.6
	github.com/ssst0n3/lightweight_api v0.7.8
	github.com/ssst0n3/lightweight_db v0.6.8
	github.com/stretchr/testify v1.6.1
)
