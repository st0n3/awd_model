package config

import (
	"github.com/ssst0n3/awesome_libs/awesome_error"
	"github.com/ssst0n3/lightweight_api/example/resource/kv_config"
	"gitlab.com/st0n3/awd_model/database"
	"gitlab.com/st0n3/awd_model/model"
	"strconv"
	"time"
)

const (
	Name          = "name"
	StartTime     = "start_time"
	EndTime       = "end_time"
	RoundInterval = "round_interval"
	OurTeamId     = "our_team_id"
)

var Event = model.Event{}

func GetName() (name string, err error) {
	name, err = kv_config.GetValueByKey(Name)
	awesome_error.CheckErr(err)
	return
}

func GetStartTime() (t time.Time, err error) {
	return database.GetTime(StartTime)
}

func GetEndTime() (t time.Time, err error) {
	return database.GetTime(EndTime)
}

func GetRoundInterval() (interval int, err error) {
	v, err := kv_config.GetValueByKey(RoundInterval)
	if err != nil {
		awesome_error.CheckErr(err)
		return
	}
	interval, err = strconv.Atoi(v)
	return
}

func SetName(name string) error {
	return database.Set(Name, name)
}

func SetStartTime(t time.Time) error {
	return database.SetTime(StartTime, t)
}

func SetEndTime(t time.Time) error {
	return database.SetTime(EndTime, t)
}

func SetRoundInterval(interval int) error {
	return database.Set(RoundInterval, strconv.Itoa(interval))
}

func CreateEmptyEvent() {
	for _, key := range []string{Name, StartTime, EndTime, RoundInterval, OurTeamId} {
		database.CreateEmpty(key, "")
	}
}

func Init() (err error) {
	CreateEmptyEvent()
	Event.StartTime, err = GetStartTime()
	if err != nil {
		awesome_error.CheckErr(err)
		return
	}
	Event.EndTime, err = GetEndTime()
	if err != nil {
		awesome_error.CheckErr(err)
		return
	}
	Event.RoundInterval, err = GetRoundInterval()
	if err != nil {
		awesome_error.CheckErr(err)
		return
	}
	return
}
